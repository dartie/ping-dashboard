table_fields = {
    'id': 'ID',
    'description': 'Description',
    'ip': 'Address IP',
    'hostname': 'Hostname',
    'status': 'status',
    'last_response': 'Last Response'
}

table_fields_devices = {
    'id': 'ID',
    'description': 'Description',
    'ip': 'Address IP',
    'hostname': 'Hostname',
    'enabled': 'Enabled'
}
