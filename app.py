from flask import Flask, render_template, request, jsonify, send_from_directory
from werkzeug.routing import BaseConverter
import os
import sys
import time
import inspect
from flask_sqlalchemy import SQLAlchemy
import datetime
import time
import threading
import json
import unidecode
import traceback
from settings import table_fields, table_fields_devices
from myping import get_hosts, resolve_hostname, get_netcard_ips, scan, run_cmd, extract_ping_info  # , ping_host

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

op_system = sys.platform

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app = Flask(__name__)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///Network.sqlite3'
db = SQLAlchemy(app)
app.url_map.converters['regex'] = RegexConverter


class Devices(db.Model):
    __tablename__ = 'devices'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text)
    ip = db.Column(db.Text)
    hostname = db.Column(db.Text)
    enabled = db.Column(db.Boolean)


class Results(db.Model):
    __tablename__ = 'results'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    device_id = db.Column(db.Integer, db.ForeignKey('devices.id'))
    status = db.Column(db.Boolean)
    last_response = db.Column(db.DateTime)


class Settings(db.Model):
    __tablename__ = 'settings'
    __table_args__ = {'extend_existing': True}
    attribute = db.Column(db.Text, primary_key=True)
    value = db.Column(db.Text)


@app.template_filter()
def format_table_fields(field):
    # adjust timestamp to show in the table
    time_format = "%m-%d-%Y, %H:%M:%S"
    formatted_field = field.strftime(time_format) if isinstance(field, datetime.datetime) else field

    return formatted_field


@app.route(r'/<regex(".*\.(png|ico)"):file>')
def favicon(file):
    file = os.path.basename(file)
    return send_from_directory(os.path.join(app.root_path, 'static', 'icons'), file, mimetype='image/vnd.microsoft.icon')


def log(text):
    datetimenow = datetime.datetime.now()
    datetimenow_str = format_table_fields(datetimenow)

    return datetimenow_str + ' - ' + text


@app.route('/discover_hosts/<settings>', methods=['POST', 'GET'])
def discover_hosts(settings):
    log_message = log('Requested a hosts discovering')
    start_discovery_time = time.time()
    print(log_message)

    settings = json.loads(settings)
    network_cards = settings['network_cards']
    only_update = settings['update_only']

    netcard__dict = get_netcard_ips()[2]

    for netcard in network_cards:
        if netcard not in netcard__dict:
            continue
        host_info = netcard__dict[netcard]

        if host_info:
            online_hosts_dict = scan(*host_info[0])
        else:
            online_hosts_dict = {}

        if only_update:
            records = Devices.query.all()
            hostnames_stored = [x.__dict__['hostname'] for x in records]  # returns a tuple for each record
            ips_stored = [x.__dict__['ip'] for x in records]  # returns a tuple for each record

            hostnames_found = []
            hostnames_found_dict = {}       # hostname : ip
            hostnames_found_inv_dict = {}   # ip : hostname
            ips_found = list(online_hosts_dict.keys())

            for ip, host_info in online_hosts_dict.items():
                hostnames_found.append(host_info[0])
                hostnames_found_dict[host_info[0]] = ip
                hostnames_found_inv_dict[ip] = host_info[0]

            new_hostnames = set(hostnames_found) - set(hostnames_stored)

            online_hosts_dict = {}
            for host_to_add in new_hostnames:
                ip = hostnames_found_dict[host_to_add]
                online_hosts_dict[ip] = (host_to_add, True)

        else:
            db.engine.execute('DELETE FROM devices;')  # excute update sql command
            db.engine.execute('DELETE FROM sqlite_sequence;')  # excute update sql command

        for hostip, host_info in online_hosts_dict.items():
            ip_address = hostip
            hostname = host_info[0]

            insert_cmd = 'INSERT INTO devices VALUES (NULL, "{hostname}", "{ip}", "{hostname}", 1)'.format(
                ip=ip_address,
                hostname=hostname)

            db.engine.execute(insert_cmd)  # excute update sql command

        db.session.commit()

    execution_time_text = "{} seconds".format(time.time() - start_discovery_time)
    log_message = log('Hosts discovering completed. It took {}'.format(execution_time_text))
    print(log_message)

    return "Discovery completed at {}".format(format_table_fields(datetime.datetime.now()))


@app.route('/update_db_settings/<settings>', methods=['POST', 'GET'])
def update_db_settings(settings):
    settings_dict = json.loads(settings)
    for attr, value in settings_dict.items():
        sql_cmd = 'UPDATE settings SET value={} WHERE attribute="{}";'.format(value, attr)
        db.engine.execute(sql_cmd)  # excute update sql command

    db.session.commit()

    return ""  # TODO to complete


@app.route('/devices', methods=['POST', 'GET'])
def devices():
    records = db.session.query(Devices.id, Devices.description, Devices.ip, Devices.hostname, Devices.enabled).all()
    records = [x for x in records]

    table_fields_db = list(table_fields.values())[:4]
    table_fields_devices = table_fields.copy()
    del table_fields_devices['status']
    del table_fields_devices['last_response']
    table_fields_devices['enabled'] = 'Enabled'

    # get network cards
    network_cards = [x.strip() for x in get_netcard_ips()[0].keys() if not x.strip() == '']
    network_cards.sort(reverse=True)

    # settings
    settings = db.session.query(Settings.attribute, Settings.value).all()
    settings_dict = {}
    for s in settings:
        settings_dict[s[0]] = s[1]

    return render_template('devices.html', table_fields_db=table_fields_db, table_fields=table_fields_devices,
                           records=records, network_cards=network_cards, settings_dict=settings_dict)


@app.route('/update_devices/<table>', methods=['POST', 'GET'])
def update_devices(table):
    table_fields_devices_inverted = dict(map(reversed, table_fields_devices.items()))

    # get current devices from the database
    current_saved_devices_ = Devices.query.all()
    current_saved_devices_ = [x for x in current_saved_devices_]  # returns a tuple for each record
    current_saved_devices = {}
    for record in current_saved_devices_:
        current_saved_devices[record.__dict__['id']] = record.__dict__

    # get the records submitted
    table = unidecode.unidecode(table)  # remove any weird character
    table = json.loads(table, strict=False)
    table_header = table['header']
    del table['header']

    # compare the database content with the html table
    for id, row in table.items():
        try:
            id = int(id)
        except:
            pass

        # format the field string for all cases (update, insert)
        update_fields_str = ''
        insert_fields_str = ''
        col = -1
        for h in table_fields_devices:
            col += 1
            value = table[str(id)][col]
            value = value.replace('%0D%0A', '\n') if isinstance(value, str) else value  # convert the carriage return json format to CRLF

            # adjust the enabled value which needs to be 0 or 1
            if str(value).strip() == 'True':
                value = 1
            elif str(value).strip() == 'False':
                value = 0
            else:
                pass

            update_fields_str += ' {}="{}",'.format(h, value)
            insert_fields_str += ' "{}",'.format(value)
        update_fields_str = update_fields_str.strip(',')
        insert_fields_str = insert_fields_str.strip(',')

        if id in current_saved_devices:
            # update the existing record
            sql_cmd = "UPDATE devices SET {} WHERE id={};".format(update_fields_str, id)

        else:
            # create new record
            sql_cmd = "INSERT INTO devices VALUES ({});".format(insert_fields_str)

        db.engine.execute(sql_cmd)  # excute insert or update sql command

    records_to_remove = set([str(x) for x in current_saved_devices.keys()]) - set(table.keys())
    for id in records_to_remove:
        delete_cmd = "DELETE FROM devices WHERE id={}".format(id)
        db.engine.execute(delete_cmd)

    db.session.commit()

    return "Changes saved {}".format(datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y, %H:%M:%S"))


def create_table_content():
    # records = db.session.query(Results).join(Devices).all()
    records = db.session.query(Devices.id, Devices.description, Devices.ip, Devices.hostname, Results.status, Results.last_response, Devices.enabled).join(Devices).filter(Devices.enabled==True)
    records_ = [x for x in records]

    # adjust fields (in particular, apply the format for date-time)
    records = []
    for line_dict in records_:
        line_dict = line_dict._asdict()
        for k, v in line_dict.items():
            line_dict[k] = format_table_fields(v)

        records.append(line_dict)

    return records


@app.route('/refresh', methods=['POST', 'GET'])
def refresh_table():
    records = create_table_content()

    return jsonify(records)


lock = threading.Lock()
results_dict = {}
def ping_host(host_info):
    host_id = host_info['id']
    host = host_info['ip'].strip()

    # Building the command. Ex: "ping -c 1 google.com"
    if op_system == 'win32':
        command = ['ping', '-a', '-n', '1', host]

        try:
            p_out = run_cmd(command)[1]

            ip, hostname, online = extract_ping_info(p_out)
        except:
            print('Error ignored:')
            traceback.print_exc()
            hostname = ''
            online = False

    else:  # op_system == 'linux':
        command = ['ping', '-c', '1', host]
        p_out_list, p_out, p_err, p_rc = run_cmd(command)

        if p_rc == 0:
            online = True

            command = ['host', host]
            p_out_host = run_cmd(command)[1]
            ip, hostname, _ = extract_ping_info(p_out_host)
        else:
            online = False
            hostname = ''

    with lock:
        results_dict[host_id] = online, datetime.datetime.now()

    return online


def refresh_hosts_status():
    while True:
        ping_threads = []
        host_list = [x for x in Devices.query.all()]
        # results_dict = {}
        for host_info in host_list:
            host_info = host_info.__dict__
            host_ip = host_info['ip']

            # status = ping_host(host_ip)  # True = Online , False = Offline
            t_ping = threading.Thread(target=ping_host, args=(host_info,))
            t_ping.start()

            ping_threads.append(t_ping)

        for t in ping_threads:
            t.join()

        # write to db
        for host_id, result in results_dict.items():
            duplicates = Results.query.filter_by(device_id=host_id).all()

            if duplicates:
                # Update
                status = result[0]
                if status:  # only update the last_response if the host is reachable.
                    update_cmd = "UPDATE Results SET status = {}, last_response = '{}' WHERE device_id={}".format(1, result[1], host_id)
                else:
                    update_cmd = "UPDATE Results SET status = {} WHERE device_id={}".format(0, host_id)

                db.engine.execute(update_cmd)
                db.session.commit()
            else:
                # New record
                new_result = Results(device_id=host_id, status=result[0], last_response=result[1])
                db.session.add(new_result)
                db.session.commit()

        time.sleep(10)


@app.route('/', methods=['POST', 'GET'])
def index():
    records = create_table_content()

    table_fields_db = list(table_fields.keys())

    # settings
    settings = db.session.query(Settings.attribute, Settings.value).all()
    settings_dict = {}
    for s in settings:
        settings_dict[s[0]] = s[1]

    return render_template('index.html', table_fields_db=table_fields_db, table_fields=table_fields, records=records, settings_dict=settings_dict)


if __name__ == '__main__':
    t = threading.Thread(target=refresh_hosts_status)  # , args=(l,)
    t.start()
    # refresh_hosts_status()
    app.run(host="0.0.0.0", debug=True)

# GUI
# TODO: scrolling down, the search box should stick
# TODO: dictionary of hostname->description for new hosts
# TODO: add Mac address
# TODO: move up and down records
# TODO: loading animation when "scan" in progress
# DONE: Discovery hosts with UPDATE: hosts will not be deleted, but added (handles temporary offline devices)
# DONE: Refresh Timeout configurable
# DONE: make the network card selectable from the webGUI
# DONE: table in db which contains a dict hostname -> Description. The table should be edited in Devices view
# DONE: sort id and address ip as number in table
# DONE: Button in devices which discovers all hosts in the network and add them to the table

