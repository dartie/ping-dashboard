import os
import sys
import re
import subprocess
import traceback
import threading
import ipaddress

op_system = sys.platform


def run_cmd(cmd):
    if op_system == 'win32':
        shell = True
        cr = '\r\n'
    else:
        cr = '\n'
        shell = False
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=shell)
    p_out, p_err = p.communicate()
    p_rc = p.returncode
    p_out = p_out.decode('utf-8')
    p_err = p_err.decode('utf-8')
    p_out_list = p_out.split(cr)

    return p_out_list, p_out, p_err, p_rc


def get_netcard_ips():
    ip_dict = {}  # { 'network-card' : [ips] }
    interface_ip_list = []
    ip_subnet__dict = {}  # { 'network-card' : [(ip, subnet)] }
    interface_ip_subnet_list = []
    new_header = False

    if sys.platform == 'win32':
        command_ipconfig = ['ipconfig']
        p_out_list = run_cmd(command_ipconfig)[0]
        p_out_list = [x for x in p_out_list if not x.strip() == '']

        for n, o in enumerate(p_out_list):
            # print(o)
            if not o.startswith(' '):
                if new_header:
                    ip_dict[new_header] = interface_ip_list
                    interface_ip_list = []

                    ip_subnet__dict[new_header] = interface_ip_subnet_list
                    interface_ip_subnet_list = []
                new_header = o.strip().strip(':')

            else:
                if 'ipv4 address' in o.lower():
                    ip = o
                    ip = ip.split(':')[1].strip()
                    interface_ip_list.append(ip)

                    # 'subnet mask'
                    subnet = p_out_list[n + 1]
                    subnet = subnet.split(':')[1].strip()
                    interface_ip_subnet_list.append((ip, subnet))

            if n == len(p_out_list) - 1:
                ip_dict[new_header] = interface_ip_list

        del ip_dict['Windows IP Configuration']

    else:  # Unix
        command_ipconfig = ['ifconfig']
        p_out_list = run_cmd(command_ipconfig)[0]

        for n, o in enumerate(p_out_list):
            # print(o)
            if not o.startswith(' '):
                if new_header:
                    ip_dict[new_header] = interface_ip_list
                    interface_ip_list = []

                    ip_subnet__dict[new_header] = interface_ip_subnet_list
                    interface_ip_subnet_list = []
                new_header = o.strip().split(':')[0]

            else:
                if 'inet' in o.lower():
                    info = o
                    info = info.strip().split(' ')
                    ip = info[1]
                    interface_ip_list.append(ip)

                    # 'subnet mask'
                    subnet = info[4]
                    interface_ip_subnet_list.append((ip, subnet))

            if n == len(p_out_list) - 1:
                ip_dict[new_header] = interface_ip_list

    # invert ip_dict
    ip_inverted_dict = {}
    for card, ips in ip_dict.items():
        for i in ips:
            ip_inverted_dict[i] = card

    return ip_dict, ip_inverted_dict, ip_subnet__dict


def get_online_hosts():
    command_arp = ['arp', '-a']
    p_out_list = run_cmd(command_arp)[0]
    p_out_list = [x for x in p_out_list if not x.strip() == '']

    online_hosts_dict = {}
    interface_ip_list = []
    new_header = False
    for n, o in enumerate(p_out_list):
        # print(o)
        if 'internet' in o.lower() and 'address' in o.lower() and 'type' in o.lower():
            continue

        if o.lower().startswith('interface'):
            if new_header:
                online_hosts_dict[new_header] = interface_ip_list
                interface_ip_list = []
            new_header = o.lower().strip('interface: ').split(' ')[0]

        else:
            ip = o
            host_info = [x for x in ip.split(' ') if not x.strip() == '']
            regex_disallowed = re.compile(r'^(01-00-5e|ff-ff-ff-ff-ff-ff)', re.I)
            if not regex_disallowed.match(host_info[1]):
                interface_ip_list.append(host_info)  # add host to the list

        if n == len(p_out_list) - 1:
            online_hosts_dict[new_header] = interface_ip_list

    return online_hosts_dict


def get_hosts(card_filter=None):
    if op_system == 'win32':  # Windows
        ip_dict, ip_inverted_dict, ip_subnet__dict = get_netcard_ips()
        online_hosts_dict = get_online_hosts()

        if card_filter:
            online_hosts_dict_filtered = {}
            for f in card_filter:
                ips = ip_dict.get(f, None)

                if ips:
                    for i in ips:
                        filter_online_hosts = online_hosts_dict.get(i, None)

                        if filter_online_hosts:
                            online_hosts_dict_filtered[i] = filter_online_hosts

            return online_hosts_dict_filtered
        else:
            return online_hosts_dict


def resolve_hostname(ip):
    hostname = 'Unknonwn'
    if op_system == 'win32':  # Windows
        hostname_cmd = ['ping', '-a', '-n', '1', ip]
        p_out_list = run_cmd(hostname_cmd)[0]
        for ol in p_out_list:
            if ol.lower().startswith('pinging'):
                hostname = ol.split('[' + ip)[0].strip('Pinging').strip()

                return hostname

    elif op_system == 'linux':  # Linux
        hostname_cmd = ['host', ip]
        p_out_list = run_cmd(hostname_cmd)[0]
        hostname = p_out_list[0].strip().strip('.').split(' ')[-1]
    else:
        pass

    return hostname


def get_network_info():
    if os.sep == '\\':
        run_cmd(['ipconfig'])
    return '192.168.1.113', '255.255.255.0'


def extract_ping_info(cmd_output):
    cmd_output = cmd_output.strip()
    if op_system == 'win32':  # Windows
        if 'unreachable' in cmd_output.lower():
            online = False  # online
        else:
            online = True  # offline

        cmd_output_split = cmd_output.split(' ')
        ip_address = cmd_output_split[2].strip('[').strip(']')
        hostname = cmd_output_split[1]

    else:  # op_system == 'linux':
        if 'not found:' in cmd_output:
            online = False
        else:
            online = True

        if online:
            hostname = cmd_output.strip().split(' ')[-1].strip().strip('.').strip()
        else:
            hostname = ''

        ip_address = cmd_output.strip().strip('Host ').split('.')
        ip_address = '{}.{}.{}.{}'.format(ip_address[3], ip_address[2], ip_address[1], ip_address[0])

    return ip_address, hostname, online


def _ping_host(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """
    host = host.strip()

    # Option for the number of packets as a function of
    param = '-n' if os.sep == '\\' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', '-a', param, '1', host]

    if os.sep == '/':
        p = subprocess.call(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if p == 0:
            online = True
        else:
            online = False

    else:
        try:
            p_out = run_cmd(command)[1]

            ip, hostname, online = extract_ping_info(p_out)
        except:
            print('Error ignored:')
            traceback.print_exc()
            status = 0
            p_out = ''
            p = ''
            hostname = ''
            online = False

        with lock:
            if online:
                online_hosts[host] = hostname, online

    return online


def ping_host(host):
    host = host.strip()

    # Building the command. Ex: "ping -c 1 google.com"
    if op_system == 'win32':
        command = ['ping', '-a', '-n', '1', host]

        try:
            p_out = run_cmd(command)[1]

            ip, hostname, online = extract_ping_info(p_out)
        except:
            print('Error ignored:')
            traceback.print_exc()
            hostname = ''
            online = False

    else:  # op_system == 'linux':
        command = ['ping', '-c', '1', host]
        p_out_list, p_out, p_err, p_rc = run_cmd(command)

        if p_rc == 0:
            online = True

            command = ['host', host]
            p_out_host = run_cmd(command)[1]
            ip, hostname, _ = extract_ping_info(p_out_host)
        else:
            online = False
            hostname = ''

    with lock:
        if online:
            online_hosts[host] = hostname, online

    return online


def scan(ip, subnet):
    net = ipaddress.ip_network('{}/{}'.format(ip, subnet), strict=False)
    ip_list = [str(x) for x in list(net.hosts())]

    thread_list = []
    for ip in ip_list:
        t = threading.Thread(target=ping_host, args=(ip,))
        thread_list.append(t)
        t.start()

    for t in thread_list:
        t.join()  # wait all threads to be completed

    return online_hosts


lock = threading.Lock()
online_hosts = {}

# get_netcard_ips()

# get_netcard_ips() -> scan -> "online_hosts_dict"