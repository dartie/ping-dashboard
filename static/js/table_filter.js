function filter_function() {
  var input, filter, table, tr, td, td_elements, i, txtValue;
  input = document.getElementById("filter");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
      td_elements = tr[i].getElementsByTagName("td");

      if (i == 0) {
          continue;  // always display the header
      }

      let display_row = false;
      for (y = 0; y < td_elements.length; y++) {
          td = td_elements[y];
          if (td) {
              txtValue = td.textContent || td.innerText;
              if (txtValue.toUpperCase().indexOf(filter) > -1) {
                  display_row = true;
              } else {
                  ;
              }
          }
      }

      // hide or display the table row (tr)
      if (display_row) {
          tr[i].style.display = "";
      } else {
          tr[i].style.display = "none";
      }

  }
}
