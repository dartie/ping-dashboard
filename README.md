# ping-dashboard

![logo](/App-Network-Monitor-icon.png "ping-dashboard")

Monitor of network devices displayed in a web interface with a network status updated constantly in background.

No page refresh is required.


## Installation
```
git clone https://gitlab.com/dartie/ping-dashboard
cd ping-dashboard
pip3 install -r requirements.txt
python3 app.py
```

## First run
1. Go to `Devices` view
1. Select your network card/s
1. Click on `Discovery hosts` -> All hosts will be displayed in the table: from the devices view with a double-click on the cells, the content can be edited and devices can be added/removed.